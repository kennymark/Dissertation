// importing node libraries
var express = require("express"),
  path = require("path"),
  bodyParser = require("body-parser"),
  cookieParser = require("cookie-parser"),
  mongoose = require("mongoose"),
  hbs = require("express-handlebars"),
  mocha = require("mocha"),
  morgan = require("morgan"), //generating requests logs
  compress = require("compression"), // to compress request ~~
  bcrypt = require("bcrypt"),
  helmet = require("helmet"),
  moment = require("moment"),
  axios = require("axios"),
  fitbit = require("fitbit-client-oauth2"),
  session = require("express-session"),
  cors = require("cors"),
  reload = require("reload"),
  fs = require("fs");


require("dotenv").config();
let mlab = process.env.mlab;
let app = express();

//connecting to DB
mongoose.connect(mlab);

//controllers
var dashboardController = require("./controllers/dashboardController");
var settingsController = require("./controllers/settingsController")

//models
let gpModel = require("./models/gp");
let commentModel = require('./models/comments')
//view engine
app.engine(
  "hbs",
  hbs({
    defaultLayout: "main",
    extname: "hbs",
    layoutsDir: path.join(__dirname, "views/layouts"),
    partialsDir: path.join(__dirname, "views/partials")
  })
);

//settig up the view engine and
app.set("view engine", "hbs");
app.set("views", "./views");
app.use("/public", express.static(path.join(__dirname, "public")));

//body-parser makes it possible to retrieve data from forms and parses that data in JSON
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
  })
);
app.use(morgan("tiny"));
app.use(cookieParser());
app.use(compress()); // this package compresses every request the front-end makes
app.use(cors()); //allows for cross domain scripting
app.use(helmet());
//setting up the port on which to launch the app
const port = process.env.PORT || 8080;
const env = process.env.NODE_ENV;

//setting up routes
app.get("/", (req, res) => {
  res.render("index", {
    title: "GPFit"
  });
});

app.get("/login", (req, res) => {
  // login route
  res.render("login", {
    title: "Login Here"
  });
});

app.get("/auth/fitbit", (req, res) => {
  res.redirect(process.env.fitbit);
});

app.get("/auth/settings", (req, res) => {
  res.redirect(process.env.fitbitsettings);
});

app.get("/dashboard", dashboardController);

app.get("/gplogin", (req, res) => {
  res.render("gplogin", {
    title: "GP Login"
  });
});

app.post("/gplogin", (req, res) => {
  var login = {
    email: req.body.gpEmail,
    password: req.body.gpPassword
  };
var user = fs.readFileSync('user.txt', 'ascii')
var parsed = JSON.parse(user)
  var Gp = gpModel;

  Gp.findOne({ email: login.email })
    .then(gp => {
      gp.comparePassword(login.password, function(err, isMatch) {
        if (err) throw err;
        //console.log(login.password, isMatch);
        res.render("gpDashboard", { title: "Your Dashboard",gp: gp , user: parsed.user });
      });
    })
    .catch(err => err);
});

app.post('/sendtoGP', (req, res)=>{
  var email = req.body.gpemail
  gpModel.findOne({email: email}, (err,docs)=>{
    err?console.log(err):
  
    console.log(docs)
  })
  res.send('Your user details will be sent to ' + email)
  

})
app.get('/gp/patient:id', (req, res)=>{
  var id = req.params.id;

  res.render("gpDashboard", { title: "Your Dashboard" });
})
app.get("/register", (req, res) => {
  res.render("register", {
    title: "Register with FitLife"
  });
});
app.post("/register", (req, res) => {
  let register = {
    name: req.body.gpName,
    email: req.body.gpEmail,
    hospital: req.body.gpHospital,
    password: req.body.gpPassword
  };

  var GP = new gpModel(register);

  GP.save()
    .then(doc => {
      console.log("Saved GP sucessfully", doc);

      res.render("gplogin", {
        title: "You have sucessfully Signed Up"
      });
    })
    .catch(err => err);
});
app.get('/settings', settingsController)

app.get("*", (req, res) => {
  res.render("error404", {
    title: "Page not availables",
    page: req.params[0].replace('/', ' ')
  });
});
app.post('/submitcomment', (req, res)=>{
  const {comment }= req.body
  console.log(comment)
  res.redirect('gplogin')
  var notes = new commentModel({
    comment: comment,
    date: moment().format("MMM Do YY") 
  })
  notes.save().then(doc=> console.log('your comment has been saved sucessfully')).catch(err=>err)
})
reload(app);

app.listen(port, () => console.log(`Server is running at port ${port}...`));

env === "production" ? port == 80 : port == 8000

