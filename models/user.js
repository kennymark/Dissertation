var mongoose = require("mongoose");

let Schema = mongoose.Schema;

let userSchema = new Schema({
  userID: {
    type: String,
    required: false
  },
  userName: {
    type: String,
    required: false
  },
  isUserpatientOf: {
    type: Schema.Types.ObjectId,
    ref: "gps",
    required: true
  },
  date: {
    default: Date
  }
});

module.exports = mongoose.model("user", userSchema);
