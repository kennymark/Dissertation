var mongoose = require('mongoose'),
	bcrypt = require('bcrypt')
	Schema = mongoose.Schema;

var gpSchema = new Schema({
	name: {
		type: String,
		required: true
	},

	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	hospital: {
		type: String,
		required: true
	},
	comments: [ { id: Schema.Types.ObjectId } ],
	date: {
		type: Date,
		default: Date.now
	},
	isDoctorOf:{
		type:String,
	}
});
//code from stackoverflow
gpSchema.pre("save", function(next) {
  var gp = this;

  // only hash the password if it has been modified (or is new)
  if (!gp.isModified("password")) return next();

  // generate a salt
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(gp.password, salt, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      gp.password = hash;
      next();
    });
  });
});

gpSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('gps', gpSchema);
