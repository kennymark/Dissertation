var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var blogSchema = new Schema({
    title: String,
    author: String,
    body: String,
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    meta: {
        votes: Number,
        favs: Number
    }
});

let blog = mongoose.model('blogs', blogSchema)
module.exports = blog;


/* a = { title: "Arrested Development", airdate: "November 2, 2003", network: "FOX" }
b = { title: "Stella", airdate: "June 28, 2005", network: "Comedy Central" }
c = { title: "Modern Family", airdate: "September 23, 2009", network: "ABC" } */