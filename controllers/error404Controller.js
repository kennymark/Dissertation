var error404 = (req, res) => {
  res.render("error404", {
    title: "Page not available"
  });
};

module.exports = error404;
