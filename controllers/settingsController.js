let express = require("express"),
  axios = require("axios"),
  fitbit = require("fitbit-client-oauth2"),
  cors = require("cors"),
  fs = require("fs"),
  token = fs.readFileSync("token.txt", "utf8");

require("dotenv").config();
const { env } = process;

let client = new fitbit(process.env.FITBIT_CI, process.env.FITBIT_CS);
var redirect_uri = "http://localhost:8000/dashboard";
const userModel = require("../models/user");
module.exports = (req, res) => {
  let userdata,
    code = req.query.code;

  function getUserProfile(token) {
    var url = "https://api.fitbit.com/1/user/-/profile.json";
    return axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        userdata = data.data;
      fs.writeFile('user.txt', JSON.stringify(userdata))
        render();
      })
      .catch(err => err.data);
  }
  getUserProfile(token);

  function render() {
    res.render("settings", {
      title: "Details / Settings Page",
      userdata: userdata
    });
  }
  console.log("Profile", userdata);
};
