let express = require("express"),
  mongoose = require("mongoose"),
  moment = require("moment"),
  axios = require("axios"),
  fitbit = require("fitbit-client-oauth2"),
  fs = require("fs"),
  cors = require("cors");

require("dotenv").config();

let app = express();

let client = new fitbit(process.env.FITBIT_CI, process.env.FITBIT_CS);
var redirect_uri = "http://localhost:8000/dashboard";

module.exports = (req, res) => {
  let today = moment().format("YYYY-MM-DD");
  let time = new Date().toDateString();
  let activityresponse;
  let token;
  let userdata;
  let WeeklySteps;
  let WeeklyDistances;
  let gpcomments;
  var User = require("./../models/user");
  const code = req.query.code;

  console.log("Params", req.query);

  if (!code) {
    res.redirect("/login");
  }

  client.getToken(code, redirect_uri).then(token => {
    client.getDailyActivitySummary(token).then(data => {
      var realtoken = token.token.access_token;
      activityresponse = data;

      getUserProfile(realtoken);
      getWeeklySteps(realtoken);
      getWeeklyDistances(realtoken);
      getAllComments()
      fs.writeFileSync("token.txt", String(realtoken), err => {
        if (err) throw err;
        //console.log("Written token to file");
      });
    });
  });

  function getUserProfile(token) {
    var url = "https://api.fitbit.com/1/user/-/profile.json";
    return axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        userdata = data.data;
      })
      .catch(err => err.data);
  }

  function getWeeklySteps(token) {
    var url = `https://api.fitbit.com/1/user/-/activities/steps/date/${today}/7d.json`;
    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        WeeklySteps = data.data;
      })
      .catch(err => console.log(err.data));
  }

  getWeeklyDistances = token => {
    var url = `https://api.fitbit.com/1/user/-/activities/distance/date/${today}/7d.json`;
    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        WeeklyDistances = data.data;
        render();
      })
      .catch(err => console.log(err));
  };

  console.log("MYProfile", userdata);
  console.log("WeeklySteps", WeeklySteps);
  console.log("WeeklyDistances", WeeklyDistances);

  function getAllComments(){
    var commentModel = require('../models/comments')
    commentModel.find({}).then(comments=>{
      gpcomments = comments
      console.log(gpcomments)
    })
    .catch(err=>err)
  }
  function render() {
    res.render("dashboard", {
      title: "Dashboard | Your Information Lives Here",
      date: time,
      data: activityresponse,
      userdata: userdata,
      weeklydistances: WeeklyDistances,
      WeeklySteps: WeeklySteps, 
      comments: gpcomments
    });
  }
};
