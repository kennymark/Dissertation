require("dotenv").config();
var axios = require("axios"),
  fs = require("fs");

var realtoken = fs.readFileSync("./token.txt" , 'ascii');
var moment = require("moment");

let today = moment().format("YYYY-MM-DD");

function Requests() {
  
  
  this.getUserProfile = token => {
    var url = "https://api.fitbit.com/1/user/-/profile.json";

    axios
      .get(url, )
      .then(data => console.log(data.data))
      .catch(err => console.log(err));
  };

  this.getWeeklySteps = token => {
    var url = `https://api.fitbit.com/1/user/-/activities/steps/date/${today}/7d.json`;
    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        console.log(data.data);
      })
      .catch(err => console.log(err));
  };

  this.getWeeklyDistances = token => {
    var url = `https://api.fitbit.com/1/user/-/activities/distance/date/${today}/7d.json`;
    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        console.log(data.data);
      })
      .catch(err => console.log(err));
  };

  this.getWeeklyCalories = token => {
    var url = `https://api.fitbit.com/1/user/-/activities/caloriesBMR/date/${today}/7d.json`;
    axios
      .get(url, { headers: { Authorization: `Bearer ${token}` } })
      .then(data => {
        console.log(data.data);
      })
      .catch(err => console.log(err));
  };
  this.getAllActivities = token => {
    var url = `https://api.fitbit.com/1/activities.json`;
    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        console.log(data.data);
      })
      .catch(err => console.log(err));
  };

  this.getBodyWeight = token => {
    var url = `https://api.fitbit.com/1/user/-/body/log/weight/date/${today}.json`;

    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        console.log(data.data);
      })
      .catch(err => console.log(err));
  };

  this.getBodyWeightGoal = token => {
    var url = `https://api.fitbit.com/1/user/-/body/log/weight/goal.json`;
    axios
      .get(url, {
        headers: { Authorization: `Bearer ${token}` }
      })
      .then(data => {
        console.log(data.data);
      })
      .catch(err => console.log(err));
  };
};


var x = new Requests();

console.log(`bodyWeightGoal ${x.getBodyWeightGoal(realtoken)}`);
console.log(`bodyWeight  ${x.getBodyWeight(realtoken)}`);
console.log(`Weekly Distance ${x.getWeeklyDistances(realtoken)}`);
console.log(`WeeklySteps  ${x.getWeeklySteps(realtoken)}`);
console.log(`WeeklyCalories  ${x.getWeeklyCalories(realtoken)}`);
