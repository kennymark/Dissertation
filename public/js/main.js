var goals = {
  steps: document.getElementById("stepsGoal").innerText,
  activeMinutes: document.getElementById("activeMinutesGoal").innerText,
  calories: document.getElementById("caloriesOutGoal").innerText,
  distance: document.getElementById("distanceGoal").innerText
};

var summary = {
  activeScore: document.getElementById("actualActiveScore").innerText,
  calories: document.getElementById("actualCaloriesOut").innerText,
  fairlyFairActiveMinutes: document.getElementById("fairlyFairActiveMinutes")
    .innerText,
  fightlyActiveMinutes: document.getElementById("actualLightlyActiveMinutes")
    .innerText,
  steps: document.getElementById("actualSteps").innerText
};

var distance = {
  total: document.querySelectorAll("#distances")[0].innerText,
  veryActive: document.querySelectorAll("#distances")[3].innerText,
  moderatelyActive: document.querySelectorAll("#distances")[4].innerText,
  lightlyActive: document.querySelectorAll("#distances")[5].innerText
};

var weeklyStepsDates = {
  day1: document.querySelectorAll("#stepDate")[0].innerText,
  day2: document.querySelectorAll("#stepDate")[1].innerText,
  day3: document.querySelectorAll("#stepDate")[2].innerText,
  day4: document.querySelectorAll("#stepDate")[3].innerText,
  day5: document.querySelectorAll("#stepDate")[4].innerText,
  day6: document.querySelectorAll("#stepDate")[5].innerText,
  day7: document.querySelectorAll("#stepDate")[6].innerText
};
var weeklyStepsValues = {
  value1: document.querySelectorAll("#stepValue")[0].innerText,
  value2: document.querySelectorAll("#stepValue")[1].innerText,
  value3: document.querySelectorAll("#stepValue")[2].innerText,
  value4: document.querySelectorAll("#stepValue")[3].innerText,
  value5: document.querySelectorAll("#stepValue")[4].innerText,
  value6: document.querySelectorAll("#stepValue")[5].innerText,
  value7: document.querySelectorAll("#stepValue")[6].innerText
};

function datify() {
  return Object.keys(weeklyStepsDates).map(function(realDate) {
    var x = new Date(weeklyStepsDates[realDate]);
    return x.toDateString();
  });
}

console.log(datify());
/* console.table(weeklyStepsDates)
console.table(weeklyStepsValues);
console.table(goals);
console.table(summary); */

var calories = new RadialProgressChart(".calories", {
  diameter: 200,
  max: parseInt(goals.calories),
  round: true,
  series: [
    {
      value: parseInt(summary.calories),
      color: {
        linearGradient: {
          x1: "0%",
          y1: "100%",
          x2: "50%",
          y2: "0%",
          spreadMethod: "pad"
        },
        stops: [
          {
            offset: "0%",
            "stop-color": "#fe08b5",
            "stop-opacity": 1
          },
          {
            offset: "100%",
            "stop-color": "#ff1410",
            "stop-opacity": 1
          }
        ]
      }
    }
  ],
  center: {
    content: [
      function(value) {
        return value;
      },
      ` OF ${parseInt(goals.calories)}CALS`
    ],
    y: 5
  }
});

var steps = new RadialProgressChart(".steps", {
  diameter: 200,
  max: parseInt(goals.steps),
  labelStart: "\uF105",
  series: [
    {
      value: parseInt(summary.steps),
      color: {
        solid: "#F7B236",
        background: "#F9CE00"
      }
    }
  ],
  center: {
    content: [
      function(value) {
        return value;
      },
      `OF ${parseInt(goals.steps.charAt(0))}000 Steps`
    ],
    y: 5
  }
});

var sleep = new RadialProgressChart(".sleepy", {
  diameter: 200,
  max: 9,
  series: [
    {
      value: 7,
      color: ["orange", "coral"]
    }
  ],
  center: {
    content: [
      function(value) {
        return value + "hrs";
      },
      " OF SLEEP"
    ],
    y: 5
  }
});


var ctx = document.getElementById("distanceChart").getContext("2d");

var myChart = new Chart(ctx, {
  type: "bar",
  data: {
    labels: ["Total", "Very Active", "Moderatively Active", "Lightly Active"],
    datasets: [
      {
        label: "Daily Distance Breakdown",
        data: [
          distance.total,
          distance.veryActive,
          distance.moderatelyActive,
          distance.lightlyActive
        ],
        backgroundColor: ["#a9eee6", "#0e9577", "#f9a1bc", "#625772"]
      }
    ]
  },
  options: {
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false
          }
        }
      ],
      yAxes: [
        {
          gridLines: {
            display: true
          }
        }
      ]
    }
  }
});


