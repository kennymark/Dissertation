function print(string) {
    return console.log(string)
}

var gp = require('./models/gp');
var blog = require('./models/blog');

require('dotenv').config();
var db = require('mongoose');
let dburl = process.env.db;
let dburl2 = process.env.db2;
let localdb = process.env.localdb;
let mlab = process.env.mlab;



db.connect(mlab)
    .then(stuff => { print('Connected to DB', stuff) })
    .catch(err => print(err))

var GP = new gp({
    name: 'Marley James',
    email: 'live@scot.com',
    hospital: 'Liverpool Medical Hospital'
})


let Blog = new blog({
    title: 'Lovely',
    author: 'Marthe Sunder',
    body: 'Jeg elsker deg',
    hidden: false,
    meta: {
        votes: 12,
        favs: 4
    }
})

Blog.save()
    .then(function (doc) {
        print(`Saved Blog sucessfully to db`)
    })
    .catch(err => err)

GP.save()
    .then(function (doc) {
        print(`Saved ${doc.name} sucessfully to db`)
        db.connection.close()
    })
    .catch(err => err)